BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `stores` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`client_id`	INTEGER NOT NULL,
	FOREIGN KEY(`client_id`) REFERENCES `clients`(`id`)
);
CREATE TABLE IF NOT EXISTS `products` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	`reference_number`	TEXT NOT NULL UNIQUE,
	`item_number`	TEXT NOT NULL,
	`colour`	TEXT,
	`description`	TEXT,
	`size`	TEXT,
	`talla`	TEXT,
	`price`	NUMERIC,
	`category_id`	INTEGER,
	FOREIGN KEY(`category_id`) REFERENCES `categories`(`id`)
);
CREATE TABLE IF NOT EXISTS `clients` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`cif`	TEXT NOT NULL,
	`contact_name`	TEXT NOT NULL,
	`address`	TEXT NOT NULL,
	`postal_code`	INTEGER NOT NULL,
	`city`	TEXT NOT NULL,
	`phone_number`	TEXT NOT NULL,
	`email`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `categories` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`description`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `activity_types` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	INTEGER NOT NULL,
	`description`	INTEGER NOT NULL
);
CREATE TABLE IF NOT EXISTS `activities` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`client_id`	INTEGER NOT NULL,
	`product_id`	INTEGER NOT NULL,
	`activity_type_id`	INTEGER NOT NULL,
	FOREIGN KEY(`client_id`) REFERENCES `clients`(`id`),
	FOREIGN KEY(`product_id`) REFERENCES `products`(`id`),
	FOREIGN KEY(`activity_type_id`) REFERENCES `activity_types`(`id`)
);
COMMIT;
