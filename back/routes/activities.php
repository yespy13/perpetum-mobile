<?php

Route::get('/activities/client/{id}', function ($id) {   
    $results = DB::select(
        'select a.id, a.client_id, c.store_name, a.product_id, p.item_name,
        sum(a.quantity) as quantity, a.date, a.price
        from activities a, clients c, products p
        where a.client_id = c.id and a.product_id = p.id and a.activity_type_id = 3 and c.id = 1
        and a.date > :fromDate and a.date < :toDate
        group by a.product_id;',
    [
        'id' => $id,
    ]);
    return response()->json($results[0], 200);
});