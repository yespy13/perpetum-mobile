<?php

Route::get('/clients', function (Request $request) {   
    $results = DB::select('select id, cif, contact_name, store_name, address, postal_code, city, phone_number, email, store_name, client_Type, commission from clients order by city');
    return response()->json($results, 200);
});

Route::get('/clients/{id}', function ($id) {   
    $results = DB::select('select id, cif, contact_name, store_name, address, postal_code, city, phone_number, email, store_name , client_Type, commission from clients where id=:id', [
        'id' => $id,
    ]);
    return response()->json($results[0], 200);
});

Route::delete('/clients/{id}', function ($id) {
    $results = DB::delete('delete from clients where id=:id',
        [
            'id' => $id,
        ]
    );
    $result = [
        'succesful' => 'Client deleted',
    ];
    return response() -> json($result, 200);
});

Route::post('/clients', function () {
    $data = request()->all();

    DB::insert(
        "insert into clients (cif, contact_name, store_name, address, postal_code, city, phone_number,
        email, client_type, commission)
        values (:cif, :contact_name, :store_name, :address, :postal_code, :city, :phone_number,
        :email, :client_type, :commission)",
        $data
    );
});

Route::put('/clients/{id}', function ($id) {
    $data = request()->all();

    $data['id'] = $id;

    DB::update(
        'update clients SET cif = :cif, 
        contact_name = :contact_name, 
        store_name = :store_name, 
        address = :address, 
        postal_code = :postal_code, 
        city = :city, 
        phone_number = :phone_number,
         email = :email, 
         client_type = :client_type, 
         commission = :commission
          where id = :id',
        $data
    );
});


















