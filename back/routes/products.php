<?php

use Illuminate\Http\Request;

Route::get('/products', function (Request $request) {   
    $results = DB::select('select * from products order by item_name');
    return response()->json($results, 200);
});

Route::get('/products/{id}', function ($id) {
    if (productNotExists($id)) {
        abort(404);
    }
    $results = DB::select('select * from products where id=:id', [
        'id' => $id,
    ]);
    return response()->json($results[0], 200);
});

Route::post('/products', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into products (reference_number, item_name, colour, description, size, talla, price, photo, category_id)
        values (:reference_number, :item_name, :colour, :description, :size, :talla, :price, :photo, :category_id)
    ",
        $data
    );
});

Route::put('/products/{id}', function ($id) {
    
    $data = request()->all();

    $data['id'] = $id;

    DB::update(
        'update products SET 
        reference_number = :reference_number, 
        item_name = :item_name, 
        colour = :colour, 
        description = :description, 
        size = :size, 
        talla = :talla,
         price = :price, 
         photo = :photo,
         category_id = :category_id
          where id = :id',
        $data
    );
    

    
});

Route::patch('/products/{id}', function ($id) {
    if (productNotExists($id)) {
        abort(404);
    }
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['id'] = $id;

    DB::update(
        'update products SET ' . join(', ', $update_statements) . ' where id = :id',
        $data
    );

    $results = DB::select('select * from products where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::delete('/products/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (productNotExists($id)) {
        abort(404);
    }

    DB::delete('delete from products where id = :id', ['id' => $id]);

    return response()->json('', 200);
});

// Laravel tiene un fallo y productga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('productNotExists')) {
    function productNotExists($id)
    {
        $results = DB::select('select * from products where id=:id', [
            'id' => $id,
        ]);

        return count($results) == 0;
    }
}