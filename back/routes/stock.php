<?php

Route::get('/stock/client/{id}', function ($id) {   
    $results = DB::select(
        'select p.id, p.item_name, sum(a.quantity * atype.factor) as stock, c.city
        from activities a, activity_types atype, products p, clients c
        where a.activity_type_id = atype.id and a.product_id = p.id and c.id = client_id and client_id = :id
        group by product_id
        order by p.item_name;', 
        [
            'id' => $id,
        ]
    );
    return response()->json($results, 200);
});

Route::post('/stock/client/{id}/deposit', function ($id) {
    $data = request()->all();

    for($i = 0; $i < count($data); $i++) {
        $commission = DB::select('select commission from clients where id = :id', 
        [
            'id' => $id,
        ]);
        $commission = $commission[0] -> commission;
        
        $pvp = DB::select('select price from products where id = :id', 
        [
            'id' => $data[$i]['chosenProduct'],
        ]);
        $pvp = $pvp[0] -> price;
        
        $price = $pvp - $pvp * ($commission / 100);
        
        DB::insert(
            'insert into activities (client_id, product_id, activity_type_id, quantity, date, price)
            values (:client_id, :product_id, 1, :quantity, :date, :price)',
            [   
                'client_id' => $id,
                'product_id' => $data[$i]['chosenProduct'],
                'quantity' => $data[$i]['quantity'],
                'date' => $data[$i]['date'],
                'price' => $price
            ]
        );
    }
});

Route::post('/stock/client/{id}/sale', function ($id) {
    $data = request()->all();
    for($i = 0; $i < count($data); $i++) {
        $commission = DB::select('select commission from clients where id = :id', 
        [
            'id' => $id,
        ]);
        $commission = $commission[0] -> commission;
        
        $pvp = DB::select('select price from products where id = :id', 
        [
            'id' => $data[$i]['chosenProduct'],
        ]);
        $pvp = $pvp[0] -> price;
        
        $price = $pvp - $pvp * ($commission / 100);
        
        DB::insert(
            'insert into activities (client_id, product_id, activity_type_id, quantity, date, price)
            values (:client_id, :product_id, 3, :quantity, :date, :price)',
            [   
                'client_id' => $id,
                'product_id' => $data[$i]['chosenProduct'],
                'quantity' => $data[$i]['quantity'],
                'date' => $data[$i]['date'],
                'price' => $price
            ]
        );
    }
});