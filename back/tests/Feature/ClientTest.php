<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ClientTest extends TestCase {
    protected function setUp(): void {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE clients (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                cif TEXT NOT NULL,
                contact_name TEXT NOT NULL,
                address TEXT NOT NULL,
                postal_code INTEGER NOT NULL,
                city TEXT NOT NULL,
                phone_number TEXT NOT NULL,
                email TEXT NOT NULL
            );
            insert into clients (cif, contact_name, address, postal_code, city, phone_number, email) values (1, 'Bego', 'En Bilbao', 48199, 'Bilbao', '944112233', 'ladebilbao@perpetum.net');"
        );
    }

    public function testGetClients() {
        $this->json('GET', 'api/clients/')
        ->assertStatus(200)
        ->assertJson([[
            "id"=> "1",
            "cif"=> "1",
            "contact_name"=> "Bego",
            "address"=> "En Bilbao",
            "postal_code"=> "48199",
            "city"=> "Bilbao",
            "phone_number"=> "944112233",
            "email"=> "ladebilbao@perpetum.net"
        ]]);
    }
}