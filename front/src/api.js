const baseUrl = "/api/";
const baseUrlProducts = baseUrl + "products/";
const baseUrlClients = baseUrl + "clients/";
const baseUrlStockByClient = baseUrl + "stock/client/";

export default {
	// Clients
	async generateClientData() {
		const result = await fetch(`${baseUrlClients}`);
		return await result.json();
	},
	async generateSingleClientData(clientId) {
		const result = await fetch(`${baseUrlClients}` + clientId);
		return await result.json();
	},
	async deleteClientData(clientId) {
		console.log("URL con id", `${baseUrlClients}` + clientId)
		await fetch(`${baseUrlClients}` + clientId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json"
				}
			}
		).then(response => response.json());
	},
	async updateClientData(client) {
		console.log("URL con id", `${baseUrlClients}` + client.id)
		await fetch(`${baseUrlClients}` + client.id,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(client)
			}
		)
	},

	async createClient(client) {
		await fetch(
			`${baseUrlClients}`,
			{
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(client)
			}
		);
	},

	// Products

	async createProduct(product) {
		await fetch(
			`${baseUrlProducts}`,
			{
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(product)
			}
		);
	},

	async updateProductData(product) {
		console.log("URL con id", `${baseUrlProducts}` + product.id)
		await fetch(`${baseUrlProducts}` + product.id,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(product)
			}
		)
	},
	async generateProductData(productId) {
		const result = await fetch(`${baseUrlProducts}` + productId);
		return await result.json();
	},
	async generateCatalogueData() {
		const result = await fetch(`${baseUrlProducts}`);
		return await result.json();
	},

	// Stock
	async getStoreStock(clientId) {
		const result = await fetch(`${baseUrlStockByClient}` + clientId);
		const resultJson = await result.json()
		return await resultJson;
	},

	async addDeposit(clientId, basket) {
		basket = this.transformArray(basket);
		await fetch(
			`${baseUrlStockByClient}` + clientId + "/deposit",
			{
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(basket)
			}
		);
	},

	async addSale(clientId, basket) {
		basket = this.transformArray(basket);
		await fetch(
			`${baseUrlStockByClient}` + clientId + "/sale",
			{
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(basket)
			}
		);
	},

	transformArray(basket) {
		var newBasket = [];
		for (var i = 0; i < basket.length; i++) {
			newBasket[i] = {
				chosenProduct: basket[i].chosenProduct.id,
				quantity: basket[i].quantity,
				price: basket[i].price,
				date: basket[i].date
			};
		}
		return newBasket;
	},
}