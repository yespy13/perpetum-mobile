import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/product/:id',
      component: function (resolve) {
        require(['@/components/Product/ProductPage.vue'], resolve);
      }
    },
    {
      path: '/clients',
      component: function (resolve) {
        require(['@/components/Client/ClientPage.vue'], resolve);
      }
    },
    {
      path: '/client/:id',
      component: function (resolve) {
        require(['@/components/SingleClient/SingleClientPage.vue'], resolve);
      }
    },
    {
      path: '/store/:id',
      component: function (resolve) {
        require(['@/components/Store/StorePage.vue'], resolve);
      }
    },
    {
      path: '/addstore',
      component: function (resolve) {
        require(['@/components/AddEditStore/AddEditStorePage.vue'], resolve);
      }
    },
    {
      path: '/editstore/:id',
      component: function (resolve) {
        require(['@/components/EditStore/EditStorePage.vue'], resolve);
      }
    },
    {
      path: '/addproduct',
      component: function (resolve) {
        require(['@/components/AddProduct/AddProductPage.vue'], resolve);
      }
    },
    {
      path: '/editproduct/:id',
      component: function (resolve) {
        require(['@/components/EditProduct/EditProductPage.vue'], resolve);
      }
    },
    {
      path: '/storesinfo',
      component: function (resolve) {
        require(['@/components/StoresInfo/StoresInfoPage.vue'], resolve);
      }
    },
    {
      path: '/catalogue',
      component: function (resolve) {
        require(['@/components/Catalogue/CataloguePage.vue'], resolve);
      }
    },
    {
      path: '/deposit/:id',
      component: function (resolve) {
        require(['@/components/Deposit/DepositPage.vue'], resolve);
      }
    },
    {
      path: '/sale/:id',
      component: function (resolve) {
        require(['@/components/Sale/SalePage.vue'], resolve);
      }
    },
    {
      path: '/activities',
      component: function (resolve) {
        require(['@/components/Movements/MovementsPage.vue'], resolve);
      }
    },
    {
      path: '/prefactura/:id',
      component: function (resolve) {
        require(['@/components/Prefactura/PrefacturaPage.vue'], resolve);
      }
    }
  ],
});

export default router