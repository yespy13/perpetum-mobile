import { shallowMount } from '@vue/test-utils'
import CatalogueItem from '@/components/Catalogue/CatalogueItem.vue'


test('Verifica que saca lo que debe', () => {
    const wrapper = shallowMount(CatalogueItem, {
        propsData: {
            productItem: {
                item_name: 'Cosica'
            }
        },
    })

    expect(wrapper.text()).toContain('Cosica');
})