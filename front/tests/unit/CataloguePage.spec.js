import { shallowMount } from '@vue/test-utils'
import CataloguePage from '@/components/Catalogue/CataloguePage.vue'
import CatalogueItem from '@/components/Catalogue/CatalogueItem.vue'
import api from '@/api'

const finishAsyncTasks = () => new Promise(setImmediate)

test("Si está vacía, no pinta la lista de productos en CataloguePage", () => {
    api.generateCatalogueData = jest.fn()
    const wrapper = shallowMount(CataloguePage, {
        data() {
            return {
                CatalogueData: []
            }
        }
    })
    const products = wrapper.findAll(CatalogueItem).wrappers
    expect(products.length).toBe(0)
})

test("pinta lista de productos en CataloguePage", () => {
    api.generateCatalogueData = jest.fn()
    const wrapper = shallowMount(CataloguePage, {
        data() {
            return {
                CatalogueData: [
                    {id: 1, itemName: "Bolso rosa con florecitas"},
                    {id: 2, itemName: "Delantal con limones"},
                    {id: 3, itemName: "Maletín azul"},
                    {id: 4, itemName: "Mantel grande verde"}
                ]
            }
        }
    })
    const Catalogue = wrapper.findAll(CatalogueItem).wrappers;
    expect(Catalogue.length).toBe(4);
})

test("Si la lista está vacía, muestra un h3.", () => {
    api.generateCatalogueData = jest.fn()
    const wrapper = shallowMount(CataloguePage, {
        data() {
            return {
                CatalogueData: []
            }
        }
    })    
    let input = wrapper.find('h3')
    expect(input.is('h3')).toBe(true)
})

test("Funciona la carga de datos en CataloguePage", async () => {
    api.generateCatalogueData = jest.fn()
    const mockCatalogue = [
        {id: 1, itemName: "Bolso rosa con florecitas"},
        {id: 2, itemName: "Delantal con limones"},
        {id: 3, itemName: "Maletín azul"},
        {id: 4, itemName: "Mantel grande verde"}
    ]
    api.generateCatalogueData.mockReturnValue(mockCatalogue);
    const wrapper = shallowMount(CataloguePage, {
        data() {
            return {
                baseUrl: "http://127.0.0.1:8000/api/products",
                product: {
                    type: Array
                }
            }
        }
    });
    await finishAsyncTasks();

    expect(api.generateCatalogueData).toHaveBeenCalled();
    expect(wrapper.vm.CatalogueData).toEqual(mockCatalogue);
})