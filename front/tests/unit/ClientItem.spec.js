import { shallowMount } from "@vue/test-utils";
import ClientItem from "@/components/Client/ClientItem.vue";

test('Verifica que saca lo que debe', () => {
    const wrapper = shallowMount(ClientItem, {
        propsData: {
            receivedClient: {
                contactName: 'Merce',
                city: "Barcelona"
            }
        },
    })

    expect(wrapper.text()).toContain('Merce (Barcelona)');
})

test('Pintar botón DELETE', () => {
    const wrapper = shallowMount(ClientItem, {
        propsData: {
            receivedClient: {
                contactName: 'Merce',
                city: "Barcelona"
            }
        },
    })
    let buttonDelete = wrapper.find('.delete-button')

    expect(buttonDelete.is('.delete-button')).toBe(true)
})

test('Ejecuta el botón DELETE', async () => {
    const wrapper = shallowMount(ClientItem, {
        propsData: {
            receivedClient: {
                contactName: 'Merce',
                city: "Barcelona"
            }
        },
    })
    expect(wrapper.emitted().click).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[0].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['.button-delete'].length).toBe(1)
    expect(wrapper.emitted()['.button-delete'][1]).toEqual([])
})