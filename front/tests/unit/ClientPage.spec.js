import { shallowMount } from "@vue/test-utils";
import ClientPage from "@/components/Client/ClientPage.vue";
import ClientItem from "@/components/Client/ClientItem.vue";

test("Si está vacía, no pinta la lista de productos en ClientPage", () => {
  const wrapper = shallowMount(ClientPage, {
      data() {
          return {
              clientData: []
          }
      }
  })
  const productList = wrapper.findAll(ClientItem).wrappers
  expect(productList.length).toBe(0)
})

test("pinta lista de productos en ClientPage", () => {
  const wrapper = shallowMount(ClientPage, {
    data() {
      return {
        clientData: [
          {
            cif: 1,
            contactName: "Begoña",
            address: "En Bilbao",
            postalCode: 48199,
            city: "Bilbao",
            phoneNumber: "944112233",
            email: "ladebilbao@perpetum.net"
          },
          {
            cif: 2,
            contactName: "Merce",
            address: "En Barcelona",
            postalCode: 33347,
            city: "Barcelona",
            phoneNumber: "931445789",
            email: "ladebarcelona@perpetum.net"
          },
          {
            cif: 3,
            contactName: "Joaquín",
            address: "En Madrid",
            postalCode: 11111,
            city: "Madrid",
            phoneNumber: "910000001",
            email: "lademadrid@perpetum.net"
          },
          {
            cif: 4,
            contactName: "Txomin",
            address: "En Donostia",
            postalCode: 47485,
            city: "Donostia",
            phoneNumber: "943147788",
            email: "ladedonos@perpetum.net"
          },
        ]
      }
    }
  })
  const productList = wrapper.findAll(ClientItem).wrappers;
  expect(productList.length).toBe(4);
})

test("Si la lista está vacía, muestra un h3.", () => {
  const wrapper = shallowMount(ClientPage, {
    data() {
      return {
        clientData: []
      }
    }
  })    
  let input = wrapper.find('h3')
  expect(input.is('h3')).toBe(true)
})