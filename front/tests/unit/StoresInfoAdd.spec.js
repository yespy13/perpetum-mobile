import { shallowMount } from '@vue/test-utils'
import StoresInfoAdd from '@/components/StoresInfo/StoresInfoAdd.vue'

test('Crear componente StoresInfoAdd.vue', () => {
    const wrapper = shallowMount(StoresInfoAdd, {
        propsData: {
            msg: 'Hola Pantalla de añadir cliente',
            newClient: {
                cif: null,
                contact_name: null,
                store_name: null,
                address: null,
                postal_code: null,
                city: null,
                phone_number: null,
                email: null,
                client_type: null
            }
        },
    })

    expect(wrapper.text()).toContain('Hola Pantalla de añadir cliente');
})

test('Pinta los input del formulario en pantalla', () => {
    const wrapper = shallowMount(StoresInfoAdd, {
        propsData: {
            msg: null,
            newClient: {
                cif: null,
                contact_name: null,
                store_name: null,
                address: null,
                postal_code: null,
                city: null,
                phone_number: null,
                email: null,
                client_type: null
            }
        }
    })
    let input = wrapper.find('input')
    expect(input.is('input')).toBe(true)
    let button = wrapper.find('button')
    expect(button.is('button')).toBe(true)
})

