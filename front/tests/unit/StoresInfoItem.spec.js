import { shallowMount } from '@vue/test-utils'
import StoresInfoItem from '@/components/StoresInfo/StoresInfoItem.vue'


test('Verifica que saca lo que debe', () => {
    const wrapper = shallowMount(StoresInfoItem, {
        propsData: {
            store: {
                name: 'Cosica'
            }
        },
    })

    expect(wrapper.text()).toContain('Cosica');
})