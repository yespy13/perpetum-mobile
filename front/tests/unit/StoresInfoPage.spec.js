import { shallowMount } from '@vue/test-utils'
import StoresInfoPage from '@/components/StoresInfo/StoresInfoPage.vue'
import StoresInfoItem from '@/components/StoresInfo/StoresInfoItem.vue'
import api from '@/api'

const finishAsyncTasks = () => new Promise(setImmediate)

test("Si está vacía, no pinta la lista de productos en StoresInfoPage", () => {
    api.generateStoreData = jest.fn()
    const wrapper = shallowMount(StoresInfoPage, {
        data() {
            return {
                stores: []
            }
        }
    })
    const storesReceived = wrapper.findAll(StoresInfoItem).wrappers;
    expect(storesReceived.length).toBe(0);
})

test("pinta lista de productos en StoresInfoPage", () => {
    api.generateStoreData = jest.fn()
    const wrapper = shallowMount(StoresInfoPage, {
        data() {
            return {
                stores: [
                    { id: 1, name: "Barcelona" },
                    { id: 2, name: "Denver" },
                    { id: 3, name: "Madrid" },
                    { id: 4, name: "Marrakech" }
                ]
            }
        }
    })
    const storesReceived = wrapper.findAll(StoresInfoItem).wrappers;
    expect(storesReceived.length).toBe(4);
})

test("Si la lista está vacía, muestra un h3.", () => {
    api.generateStoreData = jest.fn()
    const wrapper = shallowMount(StoresInfoPage, {
        data() {
            return {
                stores: []
            }
        }
    })
    let input = wrapper.find('h3')
    expect(input.is('h3')).toBe(true)
})

test("Funciona la carga de datos en StoresInfoPage", async () => {
    api.generateStoreData = jest.fn()
    const mockStores = [
        { id: 1, name: "Barcelona" },
        { id: 2, name: "Denver" },
        { id: 3, name: "Madrid" },
        { id: 4, name: "Marrakech" }
    ]
    api.generateStoreData.mockReturnValue(mockStores);
    const wrapper = shallowMount(StoresInfoPage, {
        data() {
            return {
                baseUrl: "http://127.0.0.1:8000/api/stores",
                stores: {
                    type: Array
                }
            }
        }
    });
    await finishAsyncTasks();

    expect(api.generateStoreData).toHaveBeenCalled();
    expect(wrapper.vm.stores).toEqual(mockStores);
})