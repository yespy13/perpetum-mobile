import { shallowMount } from '@vue/test-utils'
import MovementsItem from '@/components/Movements/MovementsItem.vue'

test('Crear componente MovementsItem.vue', () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            msg: '',
            movementToDo: {
                id: 1,
                movementName: 'Ventas',
            }

        },
    })

    expect(wrapper.text()).toContain('')
})


test('Testeo de Computed', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            msg: null,
            movementToDo: {
                id: 1,
                movementName: 'Ventas',
            }
        },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.movement).toContain('Ventas')
})

test('Pintar botón EDIT', () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            msg: null,
            movementToDo: {
                id: 1,
                movementName: 'Ventas',
            }
        },
    })
    let buttonEdit = wrapper.find('.edit-button')

    expect(buttonEdit.is('.edit-button')).toBe(true)
})

test('Pintar botón DELETE', () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            msg: null,
            movementToDo: {
                id: 1,
                movementName: 'Ventas',
            }
        },
    })
    let buttonDelete = wrapper.find('.delete-button')

    expect(buttonDelete.is('.delete-button')).toBe(true)
})

test.skip('Ejecuta el botón EDIT', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            msg: null,
            movementToDo: {
                id: 1,
                movementName: 'Ventas',
            }
        },
    })

    expect(wrapper.emitted().click).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[1].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['button-edit-event'].length).toBe(1)
    expect(wrapper.emitted()['button-edit-event'][0]).toEqual([1])
})

test.skip('Ejecuta el botón DELETE', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            msg: null,
            movementToDo: {
                id: 1,
                movementName: 'Ventas',
            }
        },
    })

    expect(wrapper.emitted().click).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[1].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['button-delete-event'].length).toBe(1)
    expect(wrapper.emitted()['button-delete-event'][1]).toEqual([])
})

