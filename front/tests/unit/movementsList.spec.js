import { shallowMount } from '@vue/test-utils'
import MovementsList from '@/components/Movements/MovementsList.vue'


test('Crear componente MovementsList.vue', () => {
    const wrapper = shallowMount(MovementsList, {
        propsData: {
            msg: 'Movimientost',

        }
    })

    expect(wrapper.text()).toContain("Movimientos")
})

