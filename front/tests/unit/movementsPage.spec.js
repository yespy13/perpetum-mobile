import { shallowMount } from '@vue/test-utils'
import MovementsPage from '@/components/Movements/MovementsPage.vue'
import MovementsList from '@/components/Movements/MovementsList.vue'


test('Crear componente MovementsPage.vue', () => {
    const wrapper = shallowMount(MovementsPage, {
        propsData: {
            msg: 'Hola Movimientos',

        },
    })

    expect(wrapper.text()).toContain('Hola Movimientos');
})


test("pinta lista de movimientos base en MovementsPage", () => {
    const wrapper = shallowMount(MovementsPage, {
        propsData: {
            msg: null,
        },
        data() {
            return {
                movementsData: [
                    { id: 1, movementName: "Venta" },
                    { id: 2, movementName: "Compra" },
                    { id: 3, movementName: "Cesión" },
                    { id: 4, movementName: "Pa Regalo" }
                ]
            }
        }
    })
    const List = wrapper.findAll(MovementsList).wrappers;
    expect(List.length).toBe(1);
})

test('Pintar botón VALIDAR', () => {
    const wrapper = shallowMount(MovementsPage, {
        propsData: {
            msg: null,

        },
    })
    let buttonValidate = wrapper.find('.validate-button')

    expect(buttonValidate.is('.validate-button')).toBe(true)
})

test.skip('Pinta el input de añadir movimiento nuevo', () => {
    const wrapper = shallowMount(MovementsPage, {
        propsData: {
            msg: null,
        },
    })
    let input = wrapper.find('input')
    expect(input.is('input')).toBe(true)
})