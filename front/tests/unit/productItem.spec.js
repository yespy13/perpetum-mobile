import { shallowMount } from '@vue/test-utils'
import ProductItem from '@/components/Product/ProductItem.vue'

test('Pinta los props y el watch funciona', async () => {
    const wrapper = shallowMount(ProductItem, {
        propsData: {
            receivedProduct: {
                id: 1,
                reference_number: "null",
                item_name: 'Bolso nulo',
                colour: 'Naranja y rosa',
                description: "null",
                size: "null",
                talla: "null",
                price: 17,
                photo: "null"
            },
        }
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.localProduct.item_name).toBe('Bolso nulo')
    expect(wrapper.vm.localProduct.colour).toBe('Naranja y rosa')
})

test('Al editar verificar que lo hace bien (ProductItem)', async () => {
    const wrapper = shallowMount(ProductItem, {
        propsData: {
            receivedProduct: {
                id: 1,
                reference_number: "null",
                item_name: 'Bolso nulo',
                colour: 'Naranja y rosa',
                description: "null",
                size: "null",
                talla: "null",
                price: 17,
                photo: "null"
            },
        }
    })

    const nameInput = wrapper.findAll('.itemName').wrappers
    nameInput[0].setValue('Bolso paqueto')
    const colourInput = wrapper.findAll('.itemColour').wrappers
    colourInput[0].setValue('Fernández')
    expect(wrapper.vm.localProduct.item_name).toBe('Bolso paqueto')
    expect(wrapper.vm.localProduct.colour).toBe('Fernández')
})