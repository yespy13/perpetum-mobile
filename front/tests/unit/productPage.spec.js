import { shallowMount } from '@vue/test-utils'
import ProductPage from '@/components/Product/ProductPage.vue'
import api from '@/api'

const finishAsyncTasks = () => new Promise(setImmediate)

test.skip('Crear componente ProductPage, product vacío.', () => {
    const wrapper = shallowMount(ProductPage, {
        mocks: {
            $route: {
                params: { id: 1 }
            }
        },
        data() {
            return {
                productData: {
                    type: Object
                },
                baseImgUrl: "@/assets/",
                baseUrl: "http://127.0.0.1:8000/api/products",
                completeUrl: "",
                productId: 0,
                product: {},
                productImg: ""
            }
        },
    })
    expect(wrapper.vm.product).toStrictEqual({});
})

test("Funciona la carga de datos en ProductPage", async () => {
    api.generateProductData = jest.fn()
    const mockProduct = {
        id: 1,
        reference_number: "11aa",
        item_name: "Bolso nulo",
        colour: "Gris",
        description: "Bolso que te hace mirar al más allá",
        size: "tochísimo",
        talla: "xxxl",
        price: 1.7,
        photo: null
    };
    api.generateProductData.mockReturnValue(mockProduct);
    const wrapper = shallowMount(ProductPage, {
        mocks: {
            $route: {
                params: { id: 1 }
            }
        },
        data() {
            return {
                product: {
                    id: null,
                    reference_number: null,
                    item_name: null,
                    colour: null,
                    description: null,
                    size: null,
                    talla: null,
                    price: null,
                    photo: null
                }
            }
        }
    });
    await finishAsyncTasks();

    expect(api.generateProductData).toHaveBeenCalled();
    expect(wrapper.vm.product).toEqual(mockProduct);
});