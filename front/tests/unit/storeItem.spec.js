import { shallowMount } from '@vue/test-utils'
import StoreItem from '@/components/Store/StoreItem.vue'


test('Verifica que saca lo que debe', () => {
    const wrapper = shallowMount(StoreItem, {
        propsData: {
            productInStore: {
                itemName: 'Cosica',
                itemQuantity: 23
            }
        },
    })

    expect(wrapper.text()).toContain('Cosica - ' + 23);
})

test('Comprueba que el computed que genera el producto en store funciona correctamente.', () => {
    const wrapper = shallowMount(StoreItem, {
        propsData: {
            productInStore: {
                itemName: 'Cosica',
                itemQuantity: 23
            }
        },
    })
    expect(wrapper.vm.product).toBe('Cosica - ' + 23);
})
