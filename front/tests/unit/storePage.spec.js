import { shallowMount } from '@vue/test-utils'
import StorePage from '@/components/Store/StorePage.vue'
import StoreItem from '@/components/Store/StoreItem.vue'

test("Si está vacía, no pinta la lista de productos en StorePage", () => {
    const wrapper = shallowMount(StorePage, {
        data() {
            return {
                storeData: []
            }
        }
    })
    const productList = wrapper.findAll(StoreItem).wrappers
    expect(productList.length).toBe(0)
})

test("pinta lista de productos en StorePage", () => {
    const wrapper = shallowMount(StorePage, {
        data() {
            return {
                storeData: [
                    {id: 1, itemName: "Bolso rosa con florecitas", itemQuantity: 17},
                    {id: 2, itemName: "Delantal con limones", itemQuantity: 25},
                    {id: 3, itemName: "Maletín azul", itemQuantity: 3},
                    {id: 4, itemName: "Mantel grande verde", itemQuantity: 4}
                ]
            }
        }
    })
    const productList = wrapper.findAll(StoreItem).wrappers;
    expect(productList.length).toBe(4);
})

test("Si la lista está vacía, muestra un h3.", () => {
    const wrapper = shallowMount(StorePage, {
        data() {
            return {
                storeData: []
            }
        }
    })    
    let input = wrapper.find('h3')
    expect(input.is('h3')).toBe(true)
})